Command line instructions
You can also upload existing files from your computer using the instructions below.

Git global setup
'''
git config --global user.name "Charis Prasoulas"
git config --global user.email "2712717-xprasoulas@users.noreply.gitlab.com"
'''
Create a new repository
'''
git clone git@gitlab.com:xprasoulas/visualstudiosettings.git
cd empty
git switch --create main
touch README.md
git add README.md
git commit -m "add README"
git push --set-upstream origin main
'''
Push an existing folder
cd existing_folder
git init --initial-branch=main
git remote add origin git@gitlab.com:xprasoulas/visualstudiosettings.git
git add .
git commit -m "Initial commit"
git push --set-upstream origin main
Push an existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:xprasoulas/visualstudiosettings.git
git push --set-upstream origin --all
git push --set-upstream origin --tags